
//I/O stream
//std::cout
#include <iostream>

//ROS
#include "ros/ros.h"

#include "cam_info_pub.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/CameraInfo.h"
#include <image_transport/image_transport.h>


//global variables
ros::Publisher  left_cam_info_pub_;
ros::Publisher  right_cam_info_pub_;
image_transport::Publisher left_image_pub_, right_image_pub_;

using namespace std;

void leftCamImageCallback(const sensor_msgs::Image& msg);
void rightCamImagecallback(const sensor_msgs::Image& msg);

int main(int argc,char **argv)
{

    ros::init(argc, argv, "cam_info_pub");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);


    ros::Subscriber left_cam_img_sub  = nh.subscribe("left_rgb/image_uncompressed", 1, &leftCamImageCallback);
    ros::Subscriber right_cam_img_sub = nh.subscribe("right_rgb/image_uncompressed",1, &rightCamImagecallback);

    left_image_pub_     = it.advertise("stereo/left/image_raw", 1);
    right_image_pub_    = it.advertise("stereo/right/image_raw",1);

    left_cam_info_pub_  = nh.advertise<sensor_msgs::CameraInfo>("/stereo/left/camera_info", 1);
    right_cam_info_pub_ = nh.advertise<sensor_msgs::CameraInfo>("/stereo/right/camera_info",1);


    //while(ros::ok())
    {
        ros::spin();
    }



    return 1;
}

void leftCamImageCallback(const sensor_msgs::Image &msg)
{

    sensor_msgs::CameraInfo left_cam_info;

    left_cam_info.header.stamp = msg.header.stamp;
    left_cam_info.header.frame_id = msg.header.frame_id;

    left_cam_info.height = msg.height;
    left_cam_info.width  = msg.width;

    left_cam_info.distortion_model = "plumb_bob";

    //parameters provided by slamdunk
    double d1,d2,d3,d4,d5;
    d1 = 2.3072618775059985e-03;
    d2 = -4.5187015223270658e-04;
    d3 = 3.9432257606995968e-05;
    d4 = -1.4134482640190265e-06;
    d5 = 0.000000;

    left_cam_info.D.push_back(d1);
    left_cam_info.D.push_back(d2);
    left_cam_info.D.push_back(d3);
    left_cam_info.D.push_back(d4);
    //left_cam_info.D.push_back(d5);


    left_cam_info.K.at(0) = 4.2913293735527913e+02;
    left_cam_info.K.at(2) = -9.1020644215665243e+00+(msg.width/2);
    left_cam_info.K.at(4) = 4.2913293735527913e+02;
    left_cam_info.K.at(5) = 2.9876091716615747e+00+(msg.height/2);
    left_cam_info.K.at(8) = 1.0;


    left_cam_info.R.at(0) = 9.9973720312118530e-01;
    left_cam_info.R.at(1) = 1.2039536610245705e-02;
    left_cam_info.R.at(2) = -1.9507871940732002e-02;
    left_cam_info.R.at(3) = -1.2039536610245705e-02;
    left_cam_info.R.at(4) = 9.9992752075195312e-01;
    left_cam_info.R.at(5) = 1.1744829680537805e-04;
    left_cam_info.R.at(6) = 1.9507871940732002e-02;
    left_cam_info.R.at(7) = 1.1744829680537805e-04;
    left_cam_info.R.at(8) = 9.9980968236923218e-01;

    left_cam_info.P.at(0) = 429.1329;
    left_cam_info.P.at(2) = 310.8979;
    left_cam_info.P.at(5) = 429.1329;
    left_cam_info.P.at(6) = 242.9876;
    left_cam_info.P.at(10) = 1.0;

    //parameters found with big calib board
    //    double d1,d2,d3,d4,d5;
    //    d1 = -0.293707;
    //    d2 = 0.086028;
    //    d3 = 0.000771;
    //    d4 = -0.000954;
    //    d5 = 0.000000;

    //    left_cam_info.D.push_back(d1);
    //    left_cam_info.D.push_back(d2);
    //    left_cam_info.D.push_back(d3);
    //    left_cam_info.D.push_back(d4);
    //    left_cam_info.D.push_back(d5);

    //    //    left_cam_info.D.at(0) = -0.306026;
    //    //    left_cam_info.D.at(1) = 0.109150;
    //    //    left_cam_info.D.at(2) = 0.000696;
    //    //    left_cam_info.D.at(3) = -0.003922;
    //    //    left_cam_info.D.at(4) = 0.000000;

    //    left_cam_info.K.at(0) = 430.195147;
    //    left_cam_info.K.at(2) = 314.926253;
    //    left_cam_info.K.at(4) = 430.053663;
    //    left_cam_info.K.at(5) = 242.577860;
    //    left_cam_info.K.at(8) = 1.0;

    //    left_cam_info.R.at(0) = 0.999923;
    //    left_cam_info.R.at(1) = 0.010086;
    //    left_cam_info.R.at(2) = 0.007255;
    //    left_cam_info.R.at(3) = -0.010099;
    //    left_cam_info.R.at(4) = 0.999948;
    //    left_cam_info.R.at(5) = 0.001685;
    //    left_cam_info.R.at(6) = -0.007238;
    //    left_cam_info.R.at(7) = -0.001758;
    //    left_cam_info.R.at(8) = 0.999972;

    //    left_cam_info.P.at(0) = 387.840402;
    //    left_cam_info.P.at(2) = 308.480717;
    //    left_cam_info.P.at(5) = 387.840402;
    //    left_cam_info.P.at(6) = 243.440180;
    //    left_cam_info.P.at(10) = 1.0;

    //parameters found with small calib board
    //    double d1,d2,d3,d4,d5;
    //    d1 = -0.306026;
    //    d2 = 0.109150;
    //    d3 = 0.000696;
    //    d4 = -0.003922;
    //    d5 = 0.000000;

    //    left_cam_info.D.push_back(d1);
    //    left_cam_info.D.push_back(d2);
    //    left_cam_info.D.push_back(d3);
    //    left_cam_info.D.push_back(d4);
    //    left_cam_info.D.push_back(d5);

    //    //    left_cam_info.D.at(0) = -0.306026;
    //    //    left_cam_info.D.at(1) = 0.109150;
    //    //    left_cam_info.D.at(2) = 0.000696;
    //    //    left_cam_info.D.at(3) = -0.003922;
    //    //    left_cam_info.D.at(4) = 0.000000;

    //    left_cam_info.K.at(0) = 441.040363;
    //    left_cam_info.K.at(2) = 337.004774;
    //    left_cam_info.K.at(4) = 441.754800;
    //    left_cam_info.K.at(5) = 247.885670;
    //    left_cam_info.K.at(8) = 1.0;

    //    left_cam_info.R.at(0) = 0.998365;
    //    left_cam_info.R.at(1) = 0.007769;
    //    left_cam_info.R.at(2) = 0.056631;
    //    left_cam_info.R.at(3) = -0.008132;
    //    left_cam_info.R.at(4) = 0.999948;
    //    left_cam_info.R.at(5) = 0.006186;
    //    left_cam_info.R.at(6) = -0.056580;
    //    left_cam_info.R.at(7) = -0.006637;
    //    left_cam_info.R.at(8) = 0.998376;

    //    left_cam_info.P.at(0) = 400.574610;
    //    left_cam_info.P.at(2) = 303.059223;
    //    left_cam_info.P.at(5) = 400.574610;
    //    left_cam_info.P.at(6) = 245.754980;
    //    left_cam_info.P.at(10) = 1.0;


    left_cam_info_pub_.publish(left_cam_info);
    left_image_pub_.publish(msg);

}

void rightCamImagecallback(const sensor_msgs::Image &msg)
{

    sensor_msgs::CameraInfo right_cam_info;

    right_cam_info.header.stamp = msg.header.stamp;
    right_cam_info.header.frame_id = msg.header.frame_id;

    right_cam_info.height = msg.height;
    right_cam_info.width  = msg.width;

    right_cam_info.distortion_model = "plumb_bob";

    //Parameters from slamdunk
    double d1,d2,d3,d4,d5;
    d1 = 4.3242358109222279e-03;
    d2 =  -8.3118893455722540e-04;
    d3 =  6.6650553485361446e-05;
    d4 = -1.0755378181655457e-06;
    d5 = 0.000000;

    right_cam_info.D.push_back(d1);
    right_cam_info.D.push_back(d2);
    right_cam_info.D.push_back(d3);
    right_cam_info.D.push_back(d4);
    //right_cam_info.D.push_back(d5);

    right_cam_info.K.at(0) = 4.3008595632799279e+02;
    right_cam_info.K.at(2) = -3.7148937106968560e+00+(msg.width/2);
    right_cam_info.K.at(4) = 4.3008595632799279e+02;
    right_cam_info.K.at(5) = 3.6181461815590410e+00+(msg.height/2);
    right_cam_info.K.at(8) = 1.0;

    right_cam_info.R.at(0) = 9.9990499360060381e-01;
    right_cam_info.R.at(1) = 1.3323670334715206e-02;
    right_cam_info.R.at(2) = -3.6074554044751477e-03;
    right_cam_info.R.at(3) = -1.3326042007668144e-02;
    right_cam_info.R.at(4) = 9.9991135487971738e-01;
    right_cam_info.R.at(5) = -6.3360670791738031e-04;
    right_cam_info.R.at(6) = 3.5986959106289688e-03;
    right_cam_info.R.at(7) = 6.8156792256545188e-04;
    right_cam_info.R.at(8) = 9.9999333004475655e-01;

    right_cam_info.P.at(0) = 435.0630;
    right_cam_info.P.at(1) = -0.7302;
    right_cam_info.P.at(2) = 309.4026;
    right_cam_info.P.at(3) = -303.2812;
    right_cam_info.P.at(4) = 4.4326;
    right_cam_info.P.at(5) = 429.9494;
    right_cam_info.P.at(6) = 243.8191;
    right_cam_info.P.at(7) = -3.4305;
    right_cam_info.P.at(8) = 0.0159;
    right_cam_info.P.at(9) = -0.0006;
    right_cam_info.P.at(10) = 0.9999;
    right_cam_info.P.at(11) = 0.0026;


    //Parameters found with long calib board
    //    double d1,d2,d3,d4,d5;
    //    d1 = -0.294902;
    //    d2 = 0.089984;
    //    d3 = 0.000738;
    //    d4 = 0.000540;
    //    d5 = 0.000000;

    //    right_cam_info.D.push_back(d1);
    //    right_cam_info.D.push_back(d2);
    //    right_cam_info.D.push_back(d3);
    //    right_cam_info.D.push_back(d4);
    //    right_cam_info.D.push_back(d5);


    //    right_cam_info.K.at(0) = 428.999048;
    //    right_cam_info.K.at(2) = 312.492257;
    //    right_cam_info.K.at(4) = 428.859449;
    //    right_cam_info.K.at(5) = 242.179236;
    //    right_cam_info.K.at(8) = 1.0;

    //    right_cam_info.R.at(0) = 0.999911;
    //    right_cam_info.R.at(1) = 0.011276;
    //    right_cam_info.R.at(2) = 0.007191;
    //    right_cam_info.R.at(3) = -0.011264;
    //    right_cam_info.R.at(4) = 0.999935;
    //    right_cam_info.R.at(5) = -0.001762;
    //    right_cam_info.R.at(6) = -0.007211;
    //    right_cam_info.R.at(7) = 0.001681;
    //    right_cam_info.R.at(8) = 0.999973;

    //    right_cam_info.P.at(0) = 387.840402;
    //    right_cam_info.P.at(2) = 308.480717;
    //    right_cam_info.P.at(3) = -775.917801;
    //    right_cam_info.P.at(5) = 387.840402;
    //    right_cam_info.P.at(6) = 243.440180;
    //    right_cam_info.P.at(10) = 1.0;

    //Parameters found with small calib board
    //    double d1,d2,d3,d4,d5;
    //    d1 = -0.301364;
    //    d2 =  0.095659;
    //    d3 = 0.000365;
    //    d4 = 0.000822;
    //    d5 = 0.000000;

    //    right_cam_info.D.push_back(d1);
    //    right_cam_info.D.push_back(d2);
    //    right_cam_info.D.push_back(d3);
    //    right_cam_info.D.push_back(d4);
    //    right_cam_info.D.push_back(d5);


    //    right_cam_info.K.at(0) = 439.438647;
    //    right_cam_info.K.at(2) = 312.431964;
    //    right_cam_info.K.at(4) = 438.412369;
    //    right_cam_info.K.at(5) = 242.456032;
    //    right_cam_info.K.at(8) = 1.0;

    //    right_cam_info.R.at(0) = 0.999921;
    //    right_cam_info.R.at(1) = 0.010554;
    //    right_cam_info.R.at(2) = 0.006893;
    //    right_cam_info.R.at(3) = -0.010509;
    //    right_cam_info.R.at(4) = 0.999924;
    //    right_cam_info.R.at(5) = -0.006451;
    //    right_cam_info.R.at(6) = -0.006960;
    //    right_cam_info.R.at(7) = 0.006378;
    //    right_cam_info.R.at(8) = 0.999955;

    //    right_cam_info.P.at(0) = 400.574610;
    //    right_cam_info.P.at(2) = 303.059223;
    //    right_cam_info.P.at(3) = -93.900475;
    //    right_cam_info.P.at(5) = 400.574610;
    //    right_cam_info.P.at(6) = 245.754980;
    //    right_cam_info.P.at(10) = 1.0;

    right_cam_info_pub_.publish(right_cam_info);
    right_image_pub_.publish(msg);
}
